<?php

namespace LectureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Part
 *
 * @ORM\Table(name="part")
 * @ORM\Entity(repositoryClass="LectureBundle\Repository\PartRepository")
 */
class Part
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable= true)
     */
    private $position;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Chapter",cascade={"persist"})
     * @ORM\JoinTable(name="chapter_lecture")
     */
    private $chapter;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Part
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Part
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chapter = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add chapter
     *
     * @param \LectureBundle\Entity\Chapter $chapter
     *
     * @return Part
     */
    public function addChapter(\LectureBundle\Entity\Chapter $chapter)
    {
        $this->chapter[] = $chapter;
    
        return $this;
    }

    /**
     * Remove chapter
     *
     * @param \LectureBundle\Entity\Chapter $chapter
     */
    public function removeChapter(\LectureBundle\Entity\Chapter $chapter)
    {
        $this->chapter->removeElement($chapter);
    }

    /**
     * Get chapter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChapter()
    {
        return $this->chapter;
    }
}
