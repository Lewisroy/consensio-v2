<?php

namespace LectureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chapter
 *
 * @ORM\Table(name="chapter")
 * @ORM\Entity(repositoryClass="LectureBundle\Repository\ChapterRepository")
 */
class Chapter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=355, nullable = true)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="chapterText", type="text")
     */
    private $chapterText;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Chapter
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Chapter
     */
    public function setVideo($video)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set chapterText
     *
     * @param string $chapterText
     *
     * @return Chapter
     */
    public function setChapterText($chapterText)
    {
        $this->chapterText = $chapterText;
    
        return $this;
    }

    /**
     * Get chapterText
     *
     * @return string
     */
    public function getChapterText()
    {
        return $this->chapterText;
    }
}

