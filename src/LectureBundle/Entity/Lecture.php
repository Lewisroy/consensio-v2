<?php

namespace LectureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lecture
 *
 * @ORM\Table(name="lecture")
 * @ORM\Entity(repositoryClass="LectureBundle\Repository\LectureRepository")
 */
class Lecture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="author_lecture")
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Category",cascade={"persist"})
     * @ORM\JoinTable(name="category_lecture")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Part",cascade={"persist"})
     * @ORM\JoinTable(name="part_lecture")
     */
    private $part;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=255)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255)
     */
    private $language;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Lecture
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Lecture
     */
    public function setCover($cover)
    {
        if( $cover !== null && strlen($cover) > 3)
        $this->cover = $cover;
    
        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Lecture
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->author = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->part = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add author
     *
     * @param \AU\UserBundle\Entity\User $author
     *
     * @return Lecture
     */
    public function addAuthor(\AU\UserBundle\Entity\User $author)
    {
        $this->author[] = $author;
    
        return $this;
    }

    /**
     * Remove author
     *
     * @param \AU\UserBundle\Entity\User $author
     */
    public function removeAuthor(\AU\UserBundle\Entity\User $author)
    {
        $this->author->removeElement($author);
    }

    /**
     * Get author
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * set category
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return Lecture
     */
    public function setCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category[] = $category;
    
        return $this;
    }


    /**
     * Add category
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return Lecture
     */
    public function addCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category[] = $category;
    
        return $this;
    }

    /**
     * Remove category
     *
     * @param \LectureBundle\Entity\Category $category
     */
    public function removeCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * set part
     *
     * @param \LectureBundle\Entity\Part $part
     *
     * @return Lecture
     */
    public function setPart(\LectureBundle\Entity\Part $part)
    {
        $this->part[] = $part;
    
        return $this;
    }

    /**
     * Add part
     *
     * @param \LectureBundle\Entity\Part $part
     *
     * @return Lecture
     */
    public function addPart(\LectureBundle\Entity\Part $part)
    {
        $this->part[] = $part;
    
        return $this;
    }

    /**
     * Remove part
     *
     * @param \LectureBundle\Entity\Part $part
     */
    public function removePart(\LectureBundle\Entity\Part $part)
    {
        $this->part->removeElement($part);
    }

    /**
     * Get part
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Lecture
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    
        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Lecture
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    
        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Lecture
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
