<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Part;
use LectureBundle\Entity\Chapter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Part controller.
 *
 * @Route("/{_locale}/part")
 */
class PartController extends Controller
{
    /**
     * Lists all part entities.
     *
     * @Route("/", name="part_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $parts = $em->getRepository('LectureBundle:Part')->findAll();

        return $this->render('part/index.html.twig', array(
            'parts' => $parts,
        ));
    }

    /**
     * Creates a new part entity.
     *
     * @Route("/new", name="part_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $part = new Part();
        $form = $this->createForm('LectureBundle\Form\PartType', $part);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($part);
            $em->flush($part);

            return $this->redirectToRoute('part_show', array('id' => $part->getId()));
        }

        return $this->render('part/new.html.twig', array(
            'part' => $part,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a part entity.
     *
     * @Route("/{id}", name="part_show")
     * @Method("GET")
     */
    public function showAction(Part $part)
    {
        $deleteForm = $this->createDeleteForm($part);

        return $this->render('part/show.html.twig', array(
            'part' => $part,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing part entity.
     *
     * @Route("/{id}/edit", name="part_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Part $part)
    {
        $deleteForm = $this->createDeleteForm($part);
        $editForm = $this->createForm('LectureBundle\Form\PartType', $part);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('part_edit', array('id' => $part->getId()));
        }

        return $this->render('part/edit.html.twig', array(
            'part' => $part,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Add a new chapter
     *
     * @Route("/{id}/new_chapter", name="part_add_chapter")
     * @Method({"GET", "POST"})
     */
    public function addChapterAction(Request $request, Part $part)
    {
        $chapter = new Chapter();
        $editForm = $this->createForm('LectureBundle\Form\ChapterType', $chapter);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder();   
        $lecture = $query->select('l') 
            ->from('LectureBundle:Lecture','l')
            -> leftJoin('l.part','p')
            ->where(':id = p.id')
            ->setParameters(array('id' => $part->getId()))
            ->getQuery()->getResult();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            
            $part->addChapter($chapter);
            $em->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture[0]->getId()));
        }

        return $this->render('part/chapter.html.twig', array(
            'title' => $this->get('translator')->trans('lecture.new_chapter'),
            'lecture' => $lecture[0],
            'part' => $part,
            'chapter' => $chapter,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a part entity.
     *
     * @Route("/{id}", name="part_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Part $part)
    {
        $form = $this->createDeleteForm($part);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($part);
            $em->flush($part);
        }

        return $this->redirectToRoute('part_index');
    }

    /**
     * Creates a form to delete a part entity.
     *
     * @param Part $part The part entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Part $part)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('part_delete', array('id' => $part->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
