<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Category;
use LectureBundle\Entity\Part;
use LectureBundle\Entity\Chapter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Lecture controller.
 *
 * @Route("/{_locale}/lecture")
 */
class LectureController extends Controller
{
    /**
     * Lists all lecture entities.
     *
     * @Route("/", name="lecture_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $lectures = $em->getRepository('LectureBundle:Lecture')->findAll();


        return $this->render('lecture/index.html.twig', array(
            'lectures' => $lectures,
        ));
    }

    public function returnPDFResponseFromHTML($html){
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Our Code World');
        $pdf->SetTitle(('Our Code World Title'));
        $pdf->SetSubject('Our Code World Subject');
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 11, '', true);
        //$pdf->SetMargins(20,20,40, true);
        $pdf->AddPage();
        
        $filename = 'ourcodeworld_pdf_demo';
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
}

    /**
     * Lists all category entities.
     *
     * @Route("/category", name="lecture_category_index")
     * @Method("GET")
     */
    public function indexUserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('LectureBundle:Category')->findAll();

        return $this->render('lecture/indexUser.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * @Route("/ask_validation/{id}", name="lecture_ask_validation")
     * @Method("GET")
     */
    public function validationAction(Request $request, Lecture $lecture)
    {
         if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $lecture->setStatut('OnGoingValidate');
        $em->flush();

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @Route("/download/{id}", name="lecture_download")
     * @Method("GET")
     */
    public function downloadAction(Request $request, Lecture $lecture)
    {
         if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $html = $this->render('lecture/pdf.html.twig', array(
            'lectures' => $lectures,
        ));
  
        return $this->returnPDFResponseFromHTML($html);
    }

    /**
     * @Route("/validate/{id}", name="lecture_validation")
     * @Method("GET")
     */
    public function askValidationAction(Request $request, Lecture $lecture)
    {
         if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $lecture->setStatut('Validate');
        $em->flush();

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @Route("/cancel_validation/{id}", name="lecture_cancel_validation")
     * @Method("GET")
     */
    public function cancelValidationAction(Request $request, Lecture $lecture)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $lecture->setStatut('Draft');
        $em->flush();

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * Creates a new lecture entity.
     *
     * @Route("/new", name="lecture_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $lecture = new Lecture();
        $form = $this->createForm('LectureBundle\Form\LectureType', $lecture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $lecture->addAuthor($this->getUser());
            $lecture->setDate(new \Datetime());
            $lecture->setStatut('Draft');
            if(!empty($lecture->getCover()))
            {
                $file = $lecture->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move('uploads/lecture/cover/',
                    $fileName
                );
                $lecture->setCover($fileName);
            }
            $em->persist($lecture);
            $em->flush($lecture);

            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/new.html.twig', array(
            'title' => $this->get('translator')->trans('lecture.new'),
            'lecture' => $lecture,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a lecture entity.
     *
     * @Route("/{id}", name="lecture_show")
     * @Method("GET")
     */
    public function showAction(Lecture $lecture)
    {

        $deleteForm = $this->createDeleteForm($lecture);

        return $this->render('lecture/show.html.twig', array(
            'lecture' => $lecture,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing lecture entity.
     *
     * @Route("/{id}/edit", name="lecture_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Lecture $lecture)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($lecture);
        $editForm = $this->createForm('LectureBundle\Form\LectureType', $lecture);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $uploaded_file = $editForm['cover']->getData();
            if(!empty($lecture->getCover()) && $uploaded_file)
            {
                $file = $lecture->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move('uploads/lecture/cover/',
                    $fileName
                );
                $lecture->setCover($filename);
            }
            $em->flush();

            return $this->redirectToRoute('lecture_edit', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/new.html.twig', array(
            'title' => $this->get('translator')->trans('lecture.edit'),
            'lecture' => $lecture,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Add a new part
     *
     * @Route("/{id}/new_part", name="lecture_add_part")
     * @Method({"GET", "POST"})
     */
    public function addPartAction(Request $request, Lecture $lecture)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $part = new Part();
        $editForm = $this->createForm('LectureBundle\Form\PartType', $part);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $lecture->addPart($part);
            $em->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/part.html.twig', array(
            'title' => $this->get('translator')->trans('lecture.new_part'),
            'lecture' => $lecture,
            'part' => $part,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a lecture entity.
     *
     * @Route("/{id}", name="lecture_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Lecture $lecture)
    {
        $form = $this->createDeleteForm($lecture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lecture);
            $em->flush($lecture);
        }

        return $this->redirectToRoute('lecture_index');
    }

    /**
     * Creates a form to delete a lecture entity.
     *
     * @param Lecture $lecture The lecture entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lecture $lecture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lecture_delete', array('id' => $lecture->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * search lecture
     *
     * @Route("/search/", name="lecture_search")
     * @Method({"GET", "POST"})
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $lectures = null;

        $form = $this->createFormBuilder()
            ->add('name', TextType::class , array('label' => 'Name', 'required' => false))
            ->add('language', ChoiceType::class, array(
                'choices'  => array(
                    '' => '',
                    'FR' =>'FR',
                    'EN' => 'EN'
                ), 'label' => 'language'))
            ->add('category', EntityType::class, array('label' => 'category.title', 'required' => false, 'class' => 'LectureBundle:Category','choice_label' => 'name'))
            ->add('sort', ChoiceType::class, array('label' => 'sort', 'required' => false,
                'choices'  => array(
                    'number_like' => 'number_like',
                    'older' => 'older',
                    'newer' => 'newer',
                )))
            ->add('tags', TextType::class , array('label' => 'Tags', 'required' => false))
            ->getForm();



        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $query = $em->createQueryBuilder();   
            $query->select('l') 
            ->from('LectureBundle:Lecture','l')
            -> leftJoin('l.category','c');

            $data = $form->getData();
         
            if(!empty($data['name']))
                $query->where('l.name LIKE :name or l.description LIKE :name')
                         ->setParameter('name', '%'.htmlentities($data['name']).'%');

            if(!empty($data['category']))
                $query->andwhere('c.id = :category')
                         ->setParameter('category', $data['category']->getId() );

            if(!empty($data['sort']))
                if($data['sort'] == 'older')
                    $query->orderBy('l.date', 'ASC');
                if($data['sort'] == 'newer')
                    $query->orderBy('l.date', 'DESC');

                         



            // Ajouter tri Number Like
            // Ajouter tri tags


            if(!empty($data['language']))
                $query->andwhere('l.language = :language')
                         ->setParameter('language', $data['language'] );


            $lectures = $query->getQuery()->getResult();
        }

        return $this->render('lecture/search.html.twig', array(
            'lectures' => $lectures,
            'title' => 'search',
            'form' => $form->createView(),
        ));
    }

}
