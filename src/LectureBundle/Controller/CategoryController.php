<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Category;
use LectureBundle\Entity\Lecture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 * @Route("/{_locale}/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     *
     * @Route("/", name="category_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('LectureBundle:Category')->findAll();

        return $this->render('category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Lists all category entities.
     *
     * @Route("/{id}", name="category_courses")
     * @Method("GET")
     */
    public function coursesAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder();   
        $lectures = $query->select('l') 
            ->from('LectureBundle:Lecture','l')
            -> leftJoin('l.category','c')
            ->where(':id = c.id and :statut = l.statut')
            ->setParameters(array('id' => $category->getId(), 'statut' => 'Validate'))
            ->getQuery()->getResult();

        return $this->render('category/list.html.twig', array(
            'lectures' => $lectures,
            'categorycategory' => $category
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/new", name="category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('LectureBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(!empty($category->getCover()))
            {
                $file = $category->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move('uploads/category/',
                    $fileName
                );
                $category->setCover($fileName);
            }
            $em->persist($category);
            $em->flush($category);

            return $this->redirectToRoute('category_show', array('id' => $category->getId()));
        }

        return $this->render('category/new.html.twig', array(
            'title' => 'Nouvelle catégorie',
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a category entity.
     *
     * @Route("/{id}", name="category_show")
     * @Method("GET")
     */
    public function showAction(Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('category/show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/{id}/edit", name="category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('LectureBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            if(empty($category->getCover()))
            {
                $em->clear();
                $category_tmp = $em->getRepository('LectureBundle:Category')->findOneBy(array('id'=>$category->getId()));

                $category->setCover($category_tmp->getCover());
            }
            else
            {
                $file = $category->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move('uploads/category/',
                    $fileName
                );
                $category->setCover($fileName);
                
            }
            $em->flush();
            return $this->redirectToRoute('category_edit', array('id' => $category->getId()));
        }

        return $this->render('category/new.html.twig', array(
            'title' => 'Edition catégorie',
            'category' => $category,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush($category);
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
