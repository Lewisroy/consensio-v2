<?php

namespace LectureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class LectureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('label' => 'global.array.title', 'required' => false))
        ->add('cover', FileType::class, array('label' => 'global.array.cover', 'required' => false,'data_class' => null))
        ->add('description', CKEditorType::class, array(
            'label' => 'Texte',
            'config' => array(
                'uiColor' => '#ffffff'
                )
            )) 
        ->add('language', ChoiceType::class, array(
                'choices'  => array(
                    'FR' =>'FR',
                    'EN' => 'EN'
                ), 'label' => 'language'))
        ->add('category', EntityType::class, array('label' => 'category.title', 'required' => false, 'class' => 'LectureBundle:Category','choice_label' => 'name'))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LectureBundle\Entity\Lecture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lecturebundle_lecture';
    }


}
