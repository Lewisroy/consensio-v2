<?php

namespace AU\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="AU\ProjectBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255, nullable=true)
     */
    private $cover ="";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="typeProject", type="string", length=255, nullable=true)
     */
    private $typeProject;

    /**
     * @var string
     *
     * @ORM\Column(name="home", type="string", length=255, nullable=true)
     */
    private $home;

    /**
     * @var text
     *
     * @ORM\Column(name="targetting", type="text")
     */
    private $targetting;

    /**
     * @ORM\ManyToMany(targetEntity="AU\ProjectBundle\Entity\Target",cascade={"persist"})
     * @ORM\JoinTable(name="targetting_project")
     */
    private $targets;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="project_owner")
     */
    private $owner;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Project
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Project
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set typeProject
     *
     * @param string $typeProject
     *
     * @return Project
     */
    public function setTypeProject($typeProject)
    {
        $this->typeProject = $typeProject;

        return $this;
    }

    /**
     * Get typeProject
     *
     * @return string
     */
    public function getTypeProject()
    {
        return $this->typeProject;
    }

    /**
     * Set home
     *
     * @param string $home
     *
     * @return Project
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return string
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set targetting
     *
     * @param string $targetting
     *
     * @return Project
     */
    public function setTargetting($targetting)
    {
        $this->targetting = $targetting;

        return $this;
    }

    /**
     * Get targetting
     *
     * @return string
     */
    public function getTargetting()
    {
        return $this->targetting;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->targets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add target
     *
     * @param \AU\ProjectBundle\Entity\Target $target
     *
     * @return Project
     */
    public function addTarget(\AU\ProjectBundle\Entity\Target $target)
    {
        $this->targets[] = $target;

        return $this;
    }

    /**
     * Remove target
     *
     * @param \AU\ProjectBundle\Entity\Target $target
     */
    public function removeTarget(\AU\ProjectBundle\Entity\Target $target)
    {
        $this->targets->removeElement($target);
    }

    /**
     * Get targets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargets()
    {
        return $this->targets;
    }



    /**
     * Add owner
     *
     * @param \AU\UserBundle\Entity\User $owner
     *
     * @return Project
     */
    public function addOwner(\AU\UserBundle\Entity\User $owner)
    {
        $this->owner[] = $owner;

        return $this;
    }

    /**
     * Remove owner
     *
     * @param \AU\UserBundle\Entity\User $owner
     */
    public function removeOwner(\AU\UserBundle\Entity\User $owner)
    {
        $this->owner->removeElement($owner);
    }

    /**
     * Get owner
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
