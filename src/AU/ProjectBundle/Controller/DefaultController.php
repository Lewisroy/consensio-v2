<?php

namespace AU\ProjectBundle\Controller;

use AU\AdminBundle\Entity\Page;
use AU\AdminBundle\Entity\PollAdmin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();

        $pages = $em->getRepository('AUAdminBundle:Page')->findAll();
        $polls = $em->getRepository('AUAdminBundle:PollAdmin')->findBy(array('ongoing' => true));
        

        return $this->render('AUGeneralBundle:Default:index.html.twig', array(
            'pages' => $pages,
            'polls' => $polls
        ));
    }
}
