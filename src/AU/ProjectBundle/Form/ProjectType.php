<?php

namespace AU\ProjectBundle\Form;

use AU\ProjectBundle\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'global.array.title', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'global.array.description', 'required' => false,'attr' => array('rows' => '10','cols' => '10')))
            ->add('cover', FileType::class, array('label' => 'global.array.cover', 'required' => false,'data_class' => null))
            ->add('typeProject', ChoiceType::class, array('label' => 'global.array.title', 'required' => false, 'choices'  => array(
                'global.public' => 'Public',
                'global.private' => 'Privé',
            )
            ))
            /*->add('home', ChoiceType::class, array('label' => 'Maison', 'required' => false, 'choices'  => array(
                'Planète Akmâ' => 'Planète Akmâ',
                'Planète Uni' => 'Planète Uni',
                'Planète Kilig' => 'Planète Kilig',
                'Planète Kuna' => 'Planète Kuna',
                'Planète Arysos' => 'Planète Arysos',
                'Planète Calite' => 'Planète Calite',
                'Planète Qetze' => 'Planète Qetze'

            )
            ))*/
            ->add('targetting', TextType::class, array('label' => 'global.array.target', 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AU\ProjectBundle\Entity\Project'
        ));
    }
}
