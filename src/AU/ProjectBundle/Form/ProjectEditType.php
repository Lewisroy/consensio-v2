<?php

namespace AU\ProjectBundle\Form;

use AU\ProjectBundle\Entity\Project;
use AU\ProjectBundle\Entity\Target;
use AU\ProjectBundle\Form\TargetType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProjectEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'global.array.title', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'global.array.description', 'required' => false,'attr' => array('rows' => '10','cols' => '10')))
            ->add('cover', FileType::class, array('label' => 'global.array.cover', 'required' => false,'data_class' => null))
            ->add('typeProject', ChoiceType::class, array('label' => 'global.array.statut', 'required' => false, 'choices'  => array(
                'global.public' => 'Public',
                'global.private' => 'Privé',
                
            )
            ))
            /*->add('home', ChoiceType::class, array('label' => 'Maison', 'required' => false, 'choices'  => array(
                'Planète Akmâ' => 'Planète Akmâ',
                'Planète Uni' => 'Planète Uni',
                'Planète Kilig' => 'Planète Kilig',
                'Planète Kuna' => 'Planète Kuna',
                'Planète Arysos' => 'Planète Arysos',
                'Planète Calite' => 'Planète Calite',
                'Planète Qetze' => 'Planète Qetze'
            )))*/
            ->add('targetting', TextareaType::class, array('label' => 'global.array.target', 'required' => false))
            /*->add('targets', CollectionType::class, array(
                'prototype' => true,
                'entry_type' => TargetType::class,
                'label' => " Liste d'objectif",
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'required' => false,
                
                )
            )*/
            /*->add('all_targets', ChoiceType::class, array(
                'label'    => 'Objectifs réalisés',
                'expanded' => false,
                'multiple' => true,
                'required' => false,
                'choices' => array(
                    'Jour 1' => 'Jour 1',
                    'Jour 2' => 'Jour 2',
                    'Jour 3' => 'Jour 3',
                    'Jour 4' => 'Jour 4',
                    'Jour 5' => 'Jour 5',
                    'Jour 6' => 'Jour 6',
                    'Jour 7' => 'Jour 7',
                    'Jour 8' => 'Jour 8',
                    'Jour 9' => 'Jour 9',
                    'Jour 10' => 'Jour 10',
                    'Jour 11' => 'Jour 11',
                    'Jour 12' => 'Jour 12',
                    'Jour 13' => 'Jour 13',
                    'Jour 14' => 'Jour 14',
                    'Jour 15' => 'Jour 15',
                    'Jour 16' => 'Jour 16',
                    'Jour 17' => 'Jour 17',
                    'Jour 18' => 'Jour 18',
                    'Jour 19' => 'Jour 19',
                    'Jour 20' => 'Jour 20',
                    'Jour 21' => 'Jour 21',
                    'Jour 22' => 'Jour 22',
                    'Jour 23' => 'Jour 23',
                    'Jour 24' => 'Jour 24',
                    'Jour 25' => 'Jour 25',
                    'Jour 26' => 'Jour 26',
                    'Jour 27' => 'Jour 27',
                    'Jour 28' => 'Jour 28',
                    'Jour 29' => 'Jour 29',
                    'Jour 30' => 'Jour 30',
                    'Jour 31' => 'Jour 31',



                     )
            ))*/
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AU\ProjectBundle\Entity\Project'
        ));
    }
}
