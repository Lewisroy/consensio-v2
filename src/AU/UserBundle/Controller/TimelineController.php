<?php
namespace AU\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AU\UserBundle\Entity\User;
use AU\GeneralBundle\Entity\Post;

/**
 * Timeline controller.
 *
 * @Route("/{_locale}/me")
 */
class TimelineController extends Controller
{
	/**
     * Index.
     *
     * @Route("/", name="timeline_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('AUGeneralBundle:Post')->findAll();

        return $this->render('timeline/me.html.twig');
    }

    /**
     * Add post to timeline
     *
     * @Route("/post/", name="add_post_timeline")
     * @Method({"GET", "POST"})
     */
    public function addPostToMyWall() {
    	$em = $this->getDoctrine()->getManager();
    	$message = $request->request->get('message');
    	$post = New Post();
    	$post->setMessage($message);
    	$em->persist($post);
    	$em->flush();
    }
}