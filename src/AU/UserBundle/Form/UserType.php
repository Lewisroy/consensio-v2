<?php

namespace AU\UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use AU\UserBundle\Entity\User;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array('label' => 'Prénom', 'required' => false))
            ->add('lastname', TextType::class, array('label' => 'Nom','required' => false))
            ->add('gender', ChoiceType::class, array(
                'choices'  => array(
                    'Homme' =>'Homme',
                    'Femme' => 'Femme',
                    'Agenré(e)' => 'Agenré(e)',
                    'Bigenré(e)' =>  'Bigenré(e)',
                    'Non binaire' => 'Non binaire',
                    'Genderfluid' => 'Genderfluid',
                    'Pas ton souci' => 'Pas ton souci'
                ), 'label'=> 'Genre'))
            ->add('language', TextType::class, array('label' => 'Langue','required' => false))
            ->add('location', TextType::class, array('label' => 'Ville','required' => false))
            ->add('styles', TextareaType::class, array('label' => 'Mes styles','required' => false))
            ->add('tools', TextareaType::class, array('label' => 'Mes outils','required' => false))
            ->add('equipments', TextareaType::class, array('label' => 'Mes équipements','required' => false))
            ->add('website', TextType::class, array('label' => 'website','required' => false))
            ->add('bio', TextareaType::class, array('label' => 'Biographie','required' => false))
            ->add('picture', FileType::class, array('required' => false, 'label' => "Photo de profil", 'data_class' => null))
            ->add('socialmedia', TextType::class, array('label' => 'Réseaux sociaux','required' => false))
            ->add('profileType', ChoiceType::class, array( 'required' => false,
                'choices'  => array(
                    'Ouvert' =>'Ouvert',
                    'Fermé' => 'Fermé',
                ), 'label'=> 'Mon Profil'))
            /*->add('defyMe', ChoiceType::class, array(
                'choices'  => array(
                    'Oui' =>'Oui',
                    'Non' => 'Non',
                ), 'label'=> 'Me Défier'))*/
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
        ;
        
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AU\UserBundle\Entity\User'
        ));
    }
}
