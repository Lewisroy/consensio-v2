<?php

namespace AU\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AUUserBundle extends Bundle
{
	public function getParent()
  	{
    	return 'FOSUserBundle';
  	}
}
