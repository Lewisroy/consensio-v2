<?php
// src/AU/ForumBundle/Entity/Thread.php

namespace AU\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Thread as BaseThread;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class ForumThread extends BaseThread
{
    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
    
     /** @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
    
    /** @var User
     *
     * * @ORM\ManyToOne(targetEntity="AU\UserBundle\Entity\User")
     */
    protected $author;    
    
    /**
     * Forum of this thread
     *
     * @var Forum
     * @ORM\ManyToOne(targetEntity="AU\ForumBundle\Entity\Forum")
     */
    protected $forum;
    
    public function setTitle($title)
    {
    	$this->title = $title;
    }
    
    public function getTitle()
    {
    	return $this->title;
    }
    
    public function setForum($forum)
    {
    	$this->forum = $forum;
    }
    
    public function getForum()
    {
    	return $this->forum;
    }
    
    public function setAuthor(UserInterface $author)
    {
        $this->author = $author;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getAuthorName()
    {
        if (null === $this->getAuthor()) {
            return 'Anonymous';
        }

        return $this->getAuthor()->getUsername();
    }
}
