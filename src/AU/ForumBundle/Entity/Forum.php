<?php
// src/AU/ForumBundle/Entity/Forum.php

namespace AU\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Forum
{
    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $id;
    
     /** @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;
    
     /** @var integer
     *
     * @ORM\Column(name="sortrank", type="integer")
     */
    protected $sortrank;     
    
    public function getId()
    {
    	return $this->id;
    }
    
    public function setName($name)
    {
    	$this->name = $name;
    }
    
    public function getName()
    {
    	return $this->name;
    }
    

}
