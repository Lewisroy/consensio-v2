<?php

namespace AU\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ForumController extends Controller
{
    public function indexAction()
    {
    	# index : accueil forum, affichage de la liste des forums
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('au_general_homepage');
        }
    	$em = $this->getDoctrine()->getManager();
    	$forumRepository = $em->getRepository('AUForumBundle:Forum');
    	
    	$listForums = $forumRepository->findBy(array(), array('sortrank' => 'asc'), 100, 0);
    	
        return $this->render('AUForumBundle:Forum:index.html.twig', array('listForums' => $listForums));
    }

    public function viewAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('au_general_homepage');
        }
    	# view : affichage premiere page de threads du forum $id
    	return $this->redirectToRoute('au_forum_viewpage', array('id' => $id, 'page' => 1));
    }
    
    public function viewPageAction($id, $page)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('au_general_homepage');
        }
    	# viewPage : affichage d'un forum, liste de tous ses threads par page de 20 threads 
    	$em = $this->getDoctrine()->getManager();
    	$threadRepository = $em->getRepository('AUForumBundle:ForumThread');
    	
    	$selectedForum = $em->getRepository('AUForumBundle:Forum')->find($id);
    	$threadQB = $threadRepository->createQueryBuilder('t');
    	$threadQB->select('COUNT(t)'); 
    	$threadQB->where('t.forum = :id');
    	$threadQB->setParameter('id', $id);
    	
    	$nbThread = $threadQB->getQuery()->getSingleScalarResult();

    	$nbLastPage = $nbThread % 20;
    	$nbPage = ($nbThread - $nbLastPage) / 20;
    	    
    	if ($nbLastPage > 0)
    	    { $nbPage++; }
    	if ($nbPage == 0)
    	    { $nbPage = 1;}
    	
    	if ($page > $nbPage)
    	    { $page = $nbPage; }
    	$offset = 20 * ($page - 1);
    	
    	$listThread = $threadRepository->findBy(
    	    array( 'forum' => $id ), 
    	    array('lastCommentAt' => 'desc'), 
    	    20, 
    	    $offset );
    	
    	$canStartNewThread = $this->get('security.authorization_checker')->isGranted('ROLE_USER');

    	return $this->render('AUForumBundle:Forum:view.html.twig', array(
    	    'listThread' => $listThread, 
            'page' => $page, 
            'nbPage' => $nbPage, 
            'forum' => $selectedForum,
            'canStartNewThread' => $canStartNewThread ));
    }
}
