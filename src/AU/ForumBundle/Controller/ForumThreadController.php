<?php

namespace AU\ForumBundle\Controller;

use DateTime;
use AU\ForumBundle\Entity\ForumComment;
use AU\ForumBundle\Entity\ForumThread;
use AU\ForumBundle\Form\ForumCommentType;
use AU\ForumBundle\Form\ForumFirstCommentType;
use AU\ForumBundle\Form\ForumThreadType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ForumThreadController extends Controller
{
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
    	# Accès sans id : renvoi vers home Forums
    	return $this->redirectToRoute('au_forum_homepage');
    }
	
    public function newAction($id, Request $request)
    {
    	# Interdit aux utilisateurs anonymes, redirigés
    	if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
    	{
            return $this->redirectToRoute('au_forum_viewpage', array('id' => $id, 'page' => 1));
    	}
    	
    	$newComment = new ForumComment();
    	
    	$builder = $this->get('form.factory')->createBuilder(ForumFirstCommentType::class, $newComment);
    	
    	$form = $builder->getForm();
    	
    	# Soumission premier commentaire ?
    	if ($request->isMethod('POST')){
    		$form->handleRequest($request);
    		
    		if ($form->isValid()){
    			
    			# Thread + 1er commentaire créés MAINTENANT
    			$nowDateTime = new DateTime();
    			
    			# Récupération du forum courant, à rattacher au thread
    			$em = $this->getDoctrine()->getManager();
    	        $forumRepository = $em->getRepository('AUForumBundle:Forum');
    	        $currentForum = $forumRepository->find($id);
    	        
    	        # Auteur = user connnecté
    	        $user = $this->getUser();
    			
    		    # Données complémentaires nouveau thread
    		    $newThread = $newComment->getThread();
    		    $newThread->setLastCommentAt($nowDateTime);
    		    $newThread->setForum($currentForum);
    		    $newThread->setPermaLink('/forum/thread');
    		    $newThread->setCommentable(TRUE);
    		    $newThread->setNumComments(1);
    		    $newThread->setAuthor($user);
    		    
    		    $em->persist($newThread);
    		    
    		    $em->flush();
    		    
    		    # Données complémentaires premier commentaire du thread
    		    $newComment->setCreatedAt($nowDateTime);
    		    $newComment->setAncestors(array());
    		    $newComment->setState(1);
    		    $newComment->setThread($newThread);
    		    $newComment->setAuthor($user);
    		    
    		    $em->persist($newComment);
    		    
    		    $newThread->setPermalink('/forum/thread/'.$newThread->getId());
    		    
    		    $em->persist($newThread);
    		    
    		    $em->flush();
    		    
    		    return $this->redirectToRoute('au_forum_thread_viewpage', array('id' => $newThread->getId(), 'page' => 1));
    		}
    	}    	
    	
        return $this->render('AUForumBundle:ForumThread:new.html.twig', array('form' => $form->createView() ));
    }
    
    public function viewAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
    	# view : affichage premiere page de commentaires du thread $id
    	return $this->redirectToRoute('au_forum_thread_viewpage', array('id' => $id, 'page' => 1));    	
    }

    public function viewPageAction($id, $page, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
    	# viewPage : affichage d'un thread, liste de tous ses commenaires par page de 20 commentaires 
    	$em = $this->getDoctrine()->getManager();
    	$commentRepository = $em->getRepository('AUForumBundle:ForumComment');
    	
    	$currentThread = $em->getRepository('AUForumBundle:ForumThread')->find($id);
    	$commentQB = $commentRepository->createQueryBuilder('t');
    	$commentQB->select('COUNT(t)'); 
    	$commentQB->where('t.thread = :id');
    	$commentQB->setParameter('id', $id);
    	
    	$nbComment = $commentQB->getQuery()->getSingleScalarResult();

    	$nbLastPage = $nbComment % 20;
    	$nbPage = ($nbComment - $nbLastPage) / 20;
    	    
    	if ($nbLastPage > 0)
    	    { $nbPage++; }
    	if ($nbPage == 0)
    	    { $nbPage = 1;}
    	
    	if ($page > $nbPage)
    	    { $page = $nbPage; }
    	$offset = 20 * ($page - 1);
    	
    	$listComment = $commentRepository->findBy(
    	    array( 'thread' => $id ), 
    	    array('createdAt' => 'asc'), 
    	    20, 
    	    $offset );
    	
    	$canComment = $this->get('security.authorization_checker')->isGranted('ROLE_USER');
        $formView = NULL;
    	if ($canComment){
    	    $newComment = new ForumComment();
    	
    	    $builder = $this->get('form.factory')->createBuilder(ForumCommentType::class, $newComment);
    	
    	    $form = $builder->getForm();
    	    $formView = $form->createView();
    	    
    	    # Soumission nouveau commentaire ?
    	    if ($request->isMethod('POST')){
    		    $form->handleRequest($request);
    		
    		    if ($form->isValid()){
    			
    			    # Nouveau commentaire créé MAINTENANT
    			    $nowDateTime = new DateTime();
    			
    			    # Récupération du 1er commentaire du thread
    			    $firstAncestor = $commentRepository->findOneBy(
    			        array( 'thread' => $id ),
    			        array( 'createdAt' => 'asc') );
    	            
    	            # Auteur = user connnecté
    	            $user = $this->getUser();

    		        # Données complémentaires nouveau thread
    		        $newComment->setCreatedAt($nowDateTime);
    		        $newComment->setAncestors(array($firstAncestor));
    		        $newComment->setState(1);
    		        $newComment->setThread($currentThread);
    		        $newComment->setAuthor($user);

       		        $em->persist($newComment);
    		    
                    # Mise à jour du thread
      		        $currentThread->setLastCommentAt($nowDateTime);
      		        $currentThread->incrementNumComments();
    		    
    		        $em->persist($currentThread);
    		    
    		        $em->flush();
    		    
    		        return $this->redirectToRoute('au_forum_thread_viewpage', array('id' => $currentThread->getId(), 'page' => $nbPage));
    		    }
    	    }    	
    	}

    	return $this->render('AUForumBundle:ForumThread:view.html.twig', array(
    	    'listComment' => $listComment, 
            'page' => $page, 
            'nbPage' => $nbPage, 
            'thread' => $currentThread,
            'canComment' => $canComment,
            'form' => $formView ));    	
    }
}
