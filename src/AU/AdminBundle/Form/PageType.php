<?php

namespace AU\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('label' => 'Titre', 'required' => false))
        ->add('nameEn', TextType::class, array('label' => 'Titre anglais', 'required' => false))
        ->add('text',CKEditorType::class, array(
            'label' => 'Texte',
            'config' => array(
                'uiColor' => '#ffffff'
                )
            )) 
        ->add('textEn',CKEditorType::class, array(
            'label' => 'Texte anglais',
            'config' => array(
                'uiColor' => '#ffffff'
                )
            ))       
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AU\AdminBundle\Entity\Page'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'au_adminbundle_page';
    }


}
