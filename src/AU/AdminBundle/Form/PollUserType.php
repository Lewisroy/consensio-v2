<?php

namespace AU\AdminBundle\Form;

use AU\AdminBundle\Entity\PollAdmin;
use AU\AdminBundle\Entity\PollOptions;
use AU\AdminBundle\Form\PollOptionsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class PollUserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('pollOptions', CollectionType::class, array(
            'entry_type' => PollOptionsType::class,'label' => ' ','allow_add'=> true, 'allow_delete' => true, 'prototype' => true, 'mapped' => false
        ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AU\AdminBundle\Entity\PollAdmin'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'au_adminbundle_polladmin';
    }


}
