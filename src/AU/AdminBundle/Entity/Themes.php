<?php

namespace AU\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Themes
 *
 * @ORM\Table(name="themes")
 * @ORM\Entity(repositoryClass="AU\AdminBundle\Repository\ThemesRepository")
 */
class Themes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_print", type="datetimetz", unique=true)
     */
    private $datePrint;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Themes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set datePrint
     *
     * @param \DateTime $datePrint
     *
     * @return Themes
     */
    public function setDatePrint($datePrint)
    {
        $this->datePrint = $datePrint;

        return $this;
    }

    /**
     * Get datePrint
     *
     * @return \DateTime
     */
    public function getDatePrint()
    {
        return $this->datePrint;
    }
}
