<?php

namespace AU\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="poll")
 * @ORM\Entity(repositoryClass="AU\AdminBundle\Repository\PostRepository")
 */
class PollAdmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="ongoing", type="boolean", nullable=true)
     */
    private $ongoing;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="datetimetz")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="datetimetz")
     */
    private $dateEnd;

     /**
     * @ORM\ManyToMany(targetEntity="AU\AdminBundle\Entity\PollOptions",cascade={"persist"})
     * @ORM\JoinTable(name="poll_options_users")
     */
    private $pollOptions;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Post
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Post
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ongoing
     *
     * @param boolean $ongoing
     *
     * @return Post
     */
    public function setOngoing($ongoing)
    {
        $this->ongoing = $ongoing;
    
        return $this;
    }

    /**
     * Get ongoing
     *
     * @return boolean
     */
    public function getOngoing()
    {
        return $this->ongoing;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Post
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Post
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollOptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add polloptions
     *
     * @param \AU\AdminBundle\Entity\PollOptions $polloptions
     *
     * @return PollAdmin
     */
    public function addPollOptions(\AU\AdminBundle\Entity\PollOptions $polloptions)
    {
        $this->pollOptions[] = $polloptions;
    
        return $this;
    }

    /**
     * Remove polloptions
     *
     * @param \AU\AdminBundle\Entity\PollOptions $polloptions
     */
    public function removePollOptions(\AU\AdminBundle\Entity\PollOptions $polloptions)
    {
        $this->pollOptions->removeElement($polloptions);
    }

    /**
     * Get polloptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPollOptions()
    {
        return $this->pollOptions;
    }
}
