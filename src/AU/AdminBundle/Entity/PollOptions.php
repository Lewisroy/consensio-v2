<?php

namespace AU\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PollOptions
 *
 * @ORM\Table(name="poll_options")
 * @ORM\Entity(repositoryClass="AU\AdminBundle\Repository\PollOptionsRepository")
 */
class PollOptions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="poll_voter")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PollOptions
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AU\UserBundle\Entity\User $user
     *
     * @return PollOptions
     */
    public function addUser(\AU\UserBundle\Entity\User $user)
    {
        $this->user[] = $user;
    
        return $this;
    }

    /**
     * Remove user
     *
     * @param \AU\UserBundle\Entity\User $user
     */
    public function removeUser(\AU\UserBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
