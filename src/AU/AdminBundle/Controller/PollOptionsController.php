<?php

namespace AU\AdminBundle\Controller;

use AU\AdminBundle\Entity\PollOptions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Polloption controller.
 *
 * @Route("/{_locale}/admin/polloptions")
 */
class PollOptionsController extends Controller
{
    /**
     * Lists all pollOption entities.
     *
     * @Route("/", name="poll_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $pollOptions = $em->getRepository('AUAdminBundle:PollOptions')->findAll();

        return $this->render('polloptions/index.html.twig', array(
            'pollOptions' => $pollOptions,
        ));
    }

    /**
     * Creates a new pollOption entity.
     *
     * @Route("/new", name="poll_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $pollOption = new Polloption();
        $form = $this->createForm('AU\AdminBundle\Form\PollOptionsType', $pollOption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pollOption);
            $em->flush($pollOption);

            return $this->redirectToRoute('poll_show', array('id' => $pollOption->getId()));
        }

        return $this->render('polloptions/new.html.twig', array(
            'pollOption' => $pollOption,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pollOption entity.
     *
     * @Route("/{id}", name="poll_show")
     * @Method("GET")
     */
    public function showAction(PollOptions $pollOption)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($pollOption);

        return $this->render('polloptions/show.html.twig', array(
            'pollOption' => $pollOption,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pollOption entity.
     *
     * @Route("/{id}/edit", name="poll_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PollOptions $pollOption)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($pollOption);
        $editForm = $this->createForm('AU\AdminBundle\Form\PollOptionsType', $pollOption);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('poll_edit', array('id' => $pollOption->getId()));
        }

        return $this->render('polloptions/edit.html.twig', array(
            'pollOption' => $pollOption,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pollOption entity.
     *
     * @Route("/{id}", name="poll_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PollOptions $pollOption)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createDeleteForm($pollOption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pollOption);
            $em->flush($pollOption);
        }

        return $this->redirectToRoute('poll_index');
    }

    /**
     * Creates a form to delete a pollOption entity.
     *
     * @param PollOptions $pollOption The pollOption entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PollOptions $pollOption)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('poll_delete', array('id' => $pollOption->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
