<?php

namespace AU\GeneralBundle\Controller;

use AU\GeneralBundle\Entity\Notice;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Contact controller.
 *
 * @Route("/{_locale}/notice")
 */
class NoticeController extends Controller 
{

	/**
     * Lists all pollAdmin entities.
     *
     * @Route("/", name="admin_poll_index")
     * @Method("GET")
     */
	public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $notices = $em->getRepository('AUAdminBundle:Notice')->findAll();

        return $this->render('notice/index.html.twig', array(
            'notices' => $notices,
        ));
    }

    /**
     * Add notice
     *
     * @Route("/add", name="add_notice")
     * @Method({"GET", "POST"})
     */
    public function addNoticeAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $noticeToAdd = new Notice();
        $noticeToAdd->setDateStart(new \DateTime());
        $noticeToAdd->addOwner($this->getUser());

   			$noticeToAdd->setMessages($request->request->get('notice'));
	        $em->persist($noticeToAdd);
	        $em->flush();
            $response['new_id'] = $noticeToAdd->getId();

        
        $response = array();
        $response['add'] = true;
        $em = $this->getDoctrine()->getManager();
        $response['new_id'] = $noticeToAdd->getId();
        return new JsonResponse($response);
    }

     /**
     * vote for notice
     *
     * @Route("/votenotice", name="do_vote_notice")
     * @Method("POST")
     */
    public function voteForNoticeAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $notice_id = $request->request->get('notice_id');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $notice = $em->getRepository('AUGeneralBundle:Notice')->findOneBy(array('id' => $notice_id));
        $has_vote = false;
        foreach ($notice->getVoters() as $key => $voter) {
            if($voter->getUsername() == $user->getUsername()) {
                $has_vote =true;
            }
        }


        try {
            if($has_vote)
                $notice->removeVoter($user);
            else
                $notice->addVoter($user);
            $em->persist($notice);
            $em->flush();
        }   
        catch(\Exception $e){
            error_log($e->getMessage());
        }
        
        $response = array();
        if($has_vote)
            $response['vote_add'] = false; // Si on a voté, on renvoie faux car on enlève les votes
        else
            $response['vote_add'] = true;
        return new JsonResponse($response);
    }



}