<?php

namespace AU\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AU\GeneralBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="receiver_conversation")
     */
    private $receiver;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="sender_conversation")
     */
    private $sender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="opendate", type="datetimetz")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="reportMessage", type="string", unique = false, nullable = true)
     */
    private $report;

    /**
     * @ORM\ManyToMany(targetEntity="AU\GeneralBundle\Entity\TextMessage",cascade={"persist"})
     * @ORM\JoinTable(name="text_conversation")
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="readMessage", type="string", unique = false, nullable=true)
     */
    private $read;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->message = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \DateTime('now');
        $this->report = "Non";
        $this->read = "Non Lu";
    }

    /**
     * Add message
     *
     * @param \AU\GeneralBundle\Entity\TextMessage $message
     *
     * @return Message
     */
    public function addMessage(\AU\GeneralBundle\Entity\TextMessage $message)
    {
        $this->message[] = $message;
    
        return $this;
    }

    /**
     * Remove message
     *
     * @param \AU\GeneralBundle\Entity\TextMessage $message
     */
    public function removeMessage(\AU\GeneralBundle\Entity\TextMessage $message)
    {
        $this->message->removeElement($message);
    }

    /**
     * Get message
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Add receiver
     *
     * @param \AU\UserBundle\Entity\User $receiver
     *
     * @return Message
     */
    public function addReceiver(\AU\UserBundle\Entity\User $receiver)
    {
        $this->receiver[] = $receiver;
    
        return $this;
    }

    /**
     * Remove receiver
     *
     * @param \AU\UserBundle\Entity\User $receiver
     */
    public function removeReceiver(\AU\UserBundle\Entity\User $receiver)
    {
        $this->receiver->removeElement($receiver);
    }

    /**
     * Get receiver
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Add sender
     *
     * @param \AU\UserBundle\Entity\User $sender
     *
     * @return Message
     */
    public function addSender(\AU\UserBundle\Entity\User $sender)
    {
        $this->sender[] = $sender;
    
        return $this;
    }

    /**
     * Remove sender
     *
     * @param \AU\UserBundle\Entity\User $sender
     */
    public function removeSender(\AU\UserBundle\Entity\User $sender)
    {
        $this->sender->removeElement($sender);
    }

    /**
     * Get sender
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSender()
    {
        return $this->sender;
    }

  

    /**
     * Set report
     *
     * @param string $report
     *
     * @return Message
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return string
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set read
     *
     * @param string $read
     *
     * @return Message
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return string
     */
    public function getRead()
    {
        return $this->read;
    }
}
