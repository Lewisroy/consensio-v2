<?php

namespace AU\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notice
 *
 * @ORM\Table(name="notice")
 * @ORM\Entity(repositoryClass="AU\GeneralBundle\Repository\NoticeRepository")
 */
class Notice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="messages", type="text", unique = false)
     */
    private $messages;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="notice_voter")
     */
    private $voters;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="datetime", unique=false)
     */
    private $dateStart;

    /**
     * @ORM\ManyToMany(targetEntity="AU\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="notice_owner")
     */
    private $owner;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messages
     *
     * @param string $messages
     *
     * @return Notice
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    
        return $this;
    }

    /**
     * Get messages
     *
     * @return string
     */
    public function getMessages()
    {
        return $this->messages;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->voters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Notice
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Add voter
     *
     * @param \AU\UserBundle\Entity\User $voter
     *
     * @return Notice
     */
    public function addVoter(\AU\UserBundle\Entity\User $voter)
    {
        $this->voters[] = $voter;
    
        return $this;
    }

    /**
     * Remove voter
     *
     * @param \AU\UserBundle\Entity\User $voter
     */
    public function removeVoter(\AU\UserBundle\Entity\User $voter)
    {
        $this->voters->removeElement($voter);
    }

    /**
     * Get voters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVoters()
    {
        return $this->voters;
    }

    /**
     * Set owner
     *
     * @param \AU\UserBundle\Entity\User $owner
     *
     * @return Notice
     */
    public function setOwner(\AU\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return \AU\UserBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add owner
     *
     * @param \AU\UserBundle\Entity\User $owner
     *
     * @return Notice
     */
    public function addOwner(\AU\UserBundle\Entity\User $owner)
    {
        $this->owner[] = $owner;
    
        return $this;
    }

    /**
     * Remove owner
     *
     * @param \AU\UserBundle\Entity\User $owner
     */
    public function removeOwner(\AU\UserBundle\Entity\User $owner)
    {
        $this->owner->removeElement($owner);
    }
}
