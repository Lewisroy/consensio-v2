<?php

namespace AppBundle\Controller;

use AU\AdminBundle\Entity\Page;
use AU\GeneralBundle\Entity\Notice;
use AU\AdminBundle\Entity\PollAdmin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
     /**
     * @Route("/", name="au_general_homepage")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('au_homepage');
    }

    /**
     * @Route("/{_locale}/", name="au_homepage")
     */
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pages = $em->getRepository('AUAdminBundle:Page')->findAll();
        $polls = $em->getRepository('AUAdminBundle:PollAdmin')->findBy(array('ongoing' => true));
        $notices = $em->getRepository('AUGeneralBundle:Notice')->findBy(array(),array('id' =>'desc'));
        

        return $this->render('AUGeneralBundle:Default:index.html.twig', array(
            'pages' => $pages,
            'polls' => $polls,
            'notices' => $notices
        ));
    }


}


